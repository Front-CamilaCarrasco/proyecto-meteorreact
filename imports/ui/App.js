import React from 'react';
import Formulario from "./Vistas/Formulario/Formulario";
import NavBar from './Vistas/NavBar/NavBar';


export const App = () => (
    <div>
        <NavBar/>
        <Formulario/>
    </div>
);

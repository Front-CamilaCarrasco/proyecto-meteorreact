import React, {Component} from 'react';
import {Button, Col, Form, Input, Row, Table} from 'reactstrap';
import './style.css';
import Swal from 'sweetalert2';
import {withTracker} from 'meteor/react-meteor-data';
import {medicoCollections} from '../../../api/medicos';
import {select} from '../../../api/select';

class Formulario extends Component {

    constructor(props) {
        super(props);
        this.state = {
            medicos: [],
            inputs: {
                nombres: '',
                apellido_materno: '',
                apellido_paterno: '',
                rut: '',
                especialidad: '',
            }
        };

    }

    handleSubmit = () => {
        let {
            nombres, apellido_paterno, apellido_materno, rut, especialidad
        } = this.state.inputs;
        console.log(nombres, apellido_paterno, apellido_materno, rut, especialidad);

        medicoCollections.insert({
            nombres, apellido_paterno, apellido_materno, rut, especialidad
        });

        Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Se ingreso correctamente',
            showConfirmButton: false,
            timer: 1500
        })

        this.setState({
            inputs: {
                nombres: '',
                apellido_materno: '',
                apellido_paterno: '',
                rut: '',
                especialidad: '',
            }
        });
    };

    handleInputs(input, event) {
        let inputs = this.state.inputs;
        switch (input) {
            case 'nombres':
                inputs.nombres = event.target.value;
                break;
            case 'apellido_paterno':
                inputs.apellido_paterno = event.target.value;
                break;
            case 'apellido_materno':
                inputs.apellido_materno = event.target.value;
                break;
            case 'rut':
                inputs.rut = event.target.value;
                break;
            case 'especialidad':
                inputs.especialidad = event.target.value;
                console.log(inputs.especialidad);
                break;
            default:
                break;
        }
        this.setState({inputs});
    }

    handleValidarFormulario = () => {
        let inputs = this.state.inputs;
        const Fn = {
            // Valida el rut con su cadena completa "XXXXXXXX-X"
            validaRut: function (rutCompleto) {
                rutCompleto = rutCompleto.replace("‐", "-");
                if (!/^[0-9]+[-|‐]{1}[0-9kK]{1}$/.test(rutCompleto))
                    return false;
                var tmp = rutCompleto.split('-');
                var digv = tmp[1];
                var rut = tmp[0];
                if (digv == 'K') digv = 'k';

                return (Fn.dv(rut) == digv);
            },
            dv: function (T) {
                var M = 0, S = 1;
                for (; T; T = Math.floor(T / 10))
                    S = (S + T % 10 * (9 - M++ % 6)) % 11;
                return S ? S - 1 : 'k';
            }
        };

        if (inputs['nombres'].length === 0 || inputs['nombres'].length == null || /^\s+$/.test(inputs['nombres'])) {
            alert("Debe ingresar alemenos un nombre");
            var nombre = document.getElementById("nombres");
            nombre.focus();
            return false;
        } else if (inputs['apellido_paterno'].length === 0 || inputs['apellido_paterno'].length == null || /^\s+$/.test(inputs['apellidos'])) {
            alert("Debe ingresar el apellido paterno");
            var apellido_paterno = document.getElementById("apellido_paterno");
            apellido_paterno.focus();
            return false;
        } else if (inputs['apellido_materno'].length === 0 || inputs['apellido_paterno'].length == null || /^\s+$/.test(inputs['apellidos'])) {
            alert("Debe ingresar el apellido materno");
            var apellido_materno = document.getElementById("apellido_materno");
            apellido_materno.focus();
            return false;
        } else if (inputs['especialidad'].length === 0) {
            alert("Debe seleccionar una especialidad");
            var especialidad = document.getElementById("especialidad");
            especialidad.focus();
            return false;
        } else if (Fn.validaRut(inputs['rut']).valueOf() === false) {
            alert("Debes ingresar un rut valido (Ej: 18234545-k)");
            var rut = document.getElementById("rut");
            rut.focus();
            return false;
        } else {
            this.handleSubmit();
        }

    };


    render() {
        let {
            nombres, apellido_paterno, apellido_materno, rut, especialidad
        } = this.state.inputs;
        let {medicos} = this.props;
        return (
            <div className="contenedor-padre">
                <div style={{marginBottom: '20px'}}>
                    <h4> Registrar Medico</h4>
                </div>
                <div className="contenedor-formulario">
                    <Form>
                        <Row>
                            <Col md="3" id="cont-input-label">
                                <div id="label-input-form">
                                    <label htmlFor="nombres"
                                           style={{width: '100%'}}>NOMBRES:</label>
                                </div>
                                <Input
                                    className="form-control"
                                    bsSize="sm"
                                    type="text"
                                    id="nombres"
                                    placeholder="Luis Antonio"
                                    value={nombres}
                                    onChange={e => this.handleInputs('nombres', e)}
                                />
                            </Col>
                            <Col md="3" id="cont-input-label">
                                <div id="label-input-form">
                                    <label htmlFor="apellido_paterno"
                                           style={{width: '100%'}}>APELLIDO PATERNO:</label>
                                </div>
                                <Input
                                    className="form-control"
                                    bsSize="sm"
                                    type="text"
                                    id="apellido_paterno"
                                    placeholder="Garces"
                                    value={apellido_paterno}
                                    onChange={e => this.handleInputs('apellido_paterno', e)}
                                />
                            </Col>
                            <Col md="3" id="cont-input-label">
                                <label htmlFor="apellido_materno"
                                       style={{width: '100%'}}>APELLIDO MATERNO:</label>
                                <Input
                                    className="form-control"
                                    bsSize="sm"
                                    type="text"
                                    placeholder="Maulen "
                                    id="apellido_materno"
                                    value={apellido_materno}
                                    onChange={e => this.handleInputs('apellido_materno', e)}
                                />
                            </Col>
                            <Col md="3" id="cont-input-label">
                                <div id="label-input-form">
                                    <label htmlFor="rut"
                                           style={{width: '100%'}}>RUT:</label>
                                </div>
                                <Input
                                    className="form-control"
                                    bsSize="sm"
                                    type="text"
                                    id="rut"
                                    value={rut}
                                    placeholder="12456345-k"
                                    onChange={e => this.handleInputs('rut', e)}
                                />

                            </Col>
                            <Col md="3">
                                <div id="labelART">
                                    <label htmlFor="especialidad"
                                           style={{width: '100%', marginTop:'6px'}}>ESPECIALIDAD:</label>
                                </div>
                                <Input bsSize="sm" type="select"
                                       className="form-control"
                                       id="especialidad"
                                       value={especialidad}
                                       onChange={e => this.handleInputs('especialidad', e)}
                                >
                                    <option value=""> Seleccionar</option>
                                    {select.map((element, index) =>
                                        <option key={index}
                                                value={element.nombre}>{element.nombre} </option>
                                    )}
                                </Input>
                            </Col>
                            <Col md={12} className="button-contenedor" >
                                <Button onClick={() => this.handleValidarFormulario()}>Agregar</Button>
                            </Col>

                        </Row>
                    </Form>
                </div>

                <Table className="stylo-table" bordered responsive>
                    <thead className="tableHead">
                    <tr>
                        <th > Rut</th>
                        <th> Nombres</th>
                        <th> Apellido paterno</th>
                        <th> Apellido materno</th>
                        <th> Especialidad</th>
                    </tr>
                    </thead>
                    <tbody>
                    {medicos.map((res, index) =>
                        <tr key={index}>
                            <td>{res.rut}</td>
                            <td>{res.nombres}</td>
                            <td>{res.apellido_paterno}</td>
                            <td>{res.apellido_materno}</td>
                            <td>{res.especialidad}</td>
                        </tr>
                    )}
                    </tbody>
                </Table>
            </div>
        );
    }
}


export default withTracker(() => {
    return {
        medicos: medicoCollections.find({}).fetch(),
    };
})(Formulario);

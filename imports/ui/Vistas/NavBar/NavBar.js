import React, {useState} from 'react';
import {
    Collapse,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Nav,
    Navbar,
    NavbarBrand,
    NavbarText,
    NavbarToggler,
    NavItem,
    NavLink,
    UncontrolledDropdown
} from 'reactstrap';

const Example = (props) => {
    const [isOpen, setIsOpen] = useState(false);
    const toggle = () => setIsOpen(!isOpen);

    return (
            <Navbar color="light" light expand="md" style={{borderBottom:'1px solid rgb(165 162 162)'}}>
                <NavbarBrand href="/" >
                    <img src="https://i.pinimg.com/originals/d6/b7/51/d6b751d75c50b98be47a56bd11106334.jpg"
                         width={40}
                         height={40}
                         alt="Logo"/>
                </NavbarBrand>
                <NavbarToggler onClick={toggle}/>
                <Collapse isOpen={isOpen} navbar>
                    <Nav className="mr-auto" navbar>
                        <NavItem>
                            <NavLink href="/">Medicos</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink href="/">Horas</NavLink>
                        </NavItem>

                    </Nav>
                    <NavbarText>Bienvenida /o</NavbarText>
                </Collapse>
            </Navbar>
    );
};

export default Example;

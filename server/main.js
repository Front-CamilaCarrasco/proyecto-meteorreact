import { Meteor } from "meteor/meteor";

import { medicoCollections } from "../imports/api/medicos";

function insert({ title, url }) {
    medicoCollections.insert({ title, url, createdAt: new Date() });
}

Meteor.startup(() => {
    // If the Links collection is empty, add some data.
    if (medicoCollections.find().count() === 0) {
        insert({
            nombres: "soy un medico",
        });
    }
});
